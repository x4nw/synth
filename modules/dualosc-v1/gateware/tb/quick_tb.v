`timescale 1ns/1ns

module quick_tb();

reg clk;

dualosc_bootloader dualosc_bootloader(
	.clk(clk),
	.i_rst(1'b0),
	.sdia(1'b0),
	.sdib(1'b0),
	.adc_cipo(1'b0),
	.rxd(1'b1),
	.dcv(1'b1),
	.temp2(1'b1),
	.flash_cipo(1'b0)
);

initial begin
	$dumpfile("out.vcd");
	$dumpvars(0,quick_tb);

	clk = 1'b0;
	forever begin
		#7 clk = ~clk;
	end
end

initial begin
	#320000;
	$finish;
end

endmodule
