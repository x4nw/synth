.PHONY: all doc clean mrproper test firmware

TARGET  := dualosc_bootloader
FPGA    ?= hx4k
PACKAGE ?= tq144
FREQ    ?= 64

SERV_D  := serv/rtl
SSPI_D  := simple_spi/rtl/verilog
UART_D  := uart16550

TOP     := top.v
MODULES := serv_adapter.v ram32.v ram8.v gpio.v bus_resizer.v ext_bus.v
EXT_MODULES = ${SERV} ${UART} ${SSPI}
ADDL_DEPS := firmware
TESTS   := quick_tb.v
V_INCLUDES   := ${SERV_D} ${UART_D} ${SSPI_D}

SERV = ${SERV_D}/serv_aligner.v ${SERV_D}/serv_alu.v ${SERV_D}/serv_bufreg.v \
	${SERV_D}/serv_bufreg2.v ${SERV_D}/serv_compdec.v ${SERV_D}/serv_csr.v \
	${SERV_D}/serv_ctrl.v ${SERV_D}/serv_decode.v ${SERV_D}/serv_immdec.v \
	${SERV_D}/serv_mem_if.v ${SERV_D}/serv_rf_if.v ${SERV_D}/serv_rf_ram.v \
	${SERV_D}/serv_rf_ram_if.v ${SERV_D}/serv_rf_top.v ${SERV_D}/serv_state.v \
	${SERV_D}/serv_top.v ${SERV_D}/../servile/servile_arbiter.v
UART = ${UART_D}/raminfr.v ${UART_D}/timescale.v ${UART_D}/uart_debug_if.v \
	${UART_D}/uart_defines.v ${UART_D}/uart_receiver.v ${UART_D}/uart_regs.v \
	${UART_D}/uart_rfifo.v ${UART_D}/uart_sync_flops.v ${UART_D}/uart_tfifo.v \
	${UART_D}/uart_top.v ${UART_D}/uart_transmitter.v ${UART_D}/uart_wb.v
SSPI = ${SSPI_D}/fifo4.v ${SSPI_D}/simple_spi_top.v

GREEN  := "\033[32m"
YELLOW := "\033[33m"
NORMAL := "\033[0;39m"

TOP_FILE     := $(patsubst %,v/%,${TOP})
MODULE_FILES := $(patsubst %,v/%,${MODULES}) ${EXT_MODULES}
TB_FILES     := $(patsubst %,tb/%,${TESTS})
VVP_FILES    := $(patsubst %.v,tb/%.vvp,${TESTS})
VCD_FILES    := $(patsubst %.v,tb/%.vcd,${TESTS})

# FPGA building

%.json: v/%.v ${MODULE_FILES} ${ADDL_DEPS}
	yosys -p "synth_ice40 -dff -top top -json $@" \
		$(filter %.v,$^) ${TOP_FILE} > $@.log
	printf ${YELLOW}; grep 'Warning:' $@.log; printf ${NORMAL}

%.asc: %.json %.pcf
	nextpnr-ice40 --${FPGA} --package ${PACKAGE} --freq ${FREQ} \
		--pcf $*.pcf --json $*.json --asc $@ > $@.log 2>&1 \
		|| cat $@.log
	printf ${YELLOW}; grep 'Warning:' $@.log || true
	printf ${GREEN}; awk '/Device utilisation/{p=1} /^$$/{p=0} p' $@.log
	printf ${NORMAL}

%.tim: %.asc %.pcf
	icetime -tmd ${FPGA} -p $*.pcf -c ${FREQ} $*.asc > $@
	printf ${GREEN}; grep "Total path delay" $@; printf ${NORMAL}

%.bin: %.asc
	icepack $< $@

%.c: %.bin
	srec_cat $< -Binary -Output $@ -C-Array ${TARGET}_bitstream

# systemverilog for assert
%.vvp: %.v v/${TARGET}.v ${MODULE_FILES} ${ADDL_DEPS}
	iverilog -g2005-sv -Wall -Wno-timescale -o $@ \
		$(patsubst %,-I %,${V_INCLUDES}) \
		$(filter %.v,$^)

%.vcd: %.vvp
	vvp $<
	mv out.vcd $@

all: ${TARGET}.bin ${TARGET}.tim

test.mem: test.s
	riscv64-unknown-elf-gcc -mabi=ilp32 -march=rv32i -c -O2 -ggdb $<
	riscv64-unknown-elf-objcopy -O binary test.o test.bin
	python3 serv/sw/makehex.py test.bin > $@
	#python3 -c 'f=open("test.bin", "rb"); d=f.read(); print("\n".join(f"{i:02X}" for i in d))' > test.mem
	rm test.bin test.o

install: ${TARGET}.bin
	cp ${TARGET}.bin ../src/${TARGET}.bin

flash: ${TARGET}.bin
	# Connect a programming cable to the programming port on the board,
	# making sure it shorts RSTREQ to ground to hold the FPGA in reset.
	minipro -p W25Q80DV@SOIC8 -w $< -u -s

clean:
	${RM} ${TARGET}.pdf
	${RM} ${TARGET}.json ${TARGET}.json.log
	${RM} ${TARGET}.asc  ${TARGET}.asc.log
	${RM} ${TARGET}.tim
	${RM} ${VVP_FILES} ${VCD_FILES}
	${RM} out.vcd
	${RM} test.mem
	${MAKE} -C fw clean

mrproper: clean
	${RM} ${TARGET}.bin ${TARGET}.c
	${RM} ${TARGET}.svg

test: ${VCD_FILES}

firmware:
	${MAKE} -C fw
