module ram8 #(
	parameter ADDR_WIDTH = 12,
	parameter INIT_FILE  = ""
)
(
	input  wire                  i_clk,
	input  wire                  i_rst,
	input  wire [ADDR_WIDTH-1:0] i_adr,
	input  wire [           7:0] i_dat,
	output reg  [           7:0] o_rdt,
	input  wire                  i_we,
	input  wire                  i_cyc,
	output reg                   o_ack
);

reg [7:0] mem[(2**ADDR_WIDTH) - 1:0];

initial begin
	if (INIT_FILE != "")
		$readmemh(INIT_FILE, mem, 0, (2**ADDR_WIDTH) - 1);
	o_ack <= 1'b0;
end

always @(posedge i_clk) begin
	if (i_rst) begin
		o_ack <= 1'b0;
		o_rdt <= 8'b0;
	end else if (i_cyc && !o_ack) begin
		o_ack <= 1'b1;
		o_rdt <= mem[i_adr];
		if (i_we) mem[i_adr] <= i_dat;
	end else begin
		o_ack <= 1'b0;
	end
end

endmodule
