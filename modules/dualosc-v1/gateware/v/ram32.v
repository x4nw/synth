module ram32 #(
	parameter ADDR_WIDTH = 12,
	parameter INIT_FILE  = ""
)
(
	input  wire                  i_clk,
	input  wire                  i_rst,
	input  wire [ADDR_WIDTH-1:0] i_adr,
	input  wire [          31:0] i_dat,
	input  wire [           3:0] i_sel,
	output reg  [          31:0] o_rdt,
	input  wire                  i_we,
	input  wire                  i_cyc,
	output reg                   o_ack
);

reg [31:0] mem[(2**(ADDR_WIDTH-2)) - 1:0];
wire [ADDR_WIDTH-3:0] short_addr = i_adr[ADDR_WIDTH-1:2];

initial begin
	if (INIT_FILE != "")
		$readmemh(INIT_FILE, mem, 0, (2**(ADDR_WIDTH-2)) - 1);
	o_ack <= 1'b0;
end

always @(posedge i_clk) begin
	if (i_rst) begin
		o_ack <= 1'b0;
		o_rdt <= 32'b0;
	end else if (i_cyc && !o_ack) begin
		o_ack <= 1'b1;
		o_rdt <= mem[short_addr];
		if (i_we && i_sel[0]) mem[short_addr][ 7: 0] <= i_dat[ 7: 0];
		if (i_we && i_sel[1]) mem[short_addr][15: 8] <= i_dat[15: 8];
		if (i_we && i_sel[2]) mem[short_addr][23:16] <= i_dat[23:16];
		if (i_we && i_sel[3]) mem[short_addr][31:24] <= i_dat[31:24];
	end else begin
		o_ack <= 1'b0;
	end
end

endmodule
