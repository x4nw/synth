// 32-bit GPIO module, styled after the newer AVR GPIOs

module gpio
(
	input  wire        i_clk,
	input  wire        i_rst,
	input  wire [ 5:0] i_adr,
	input  wire [31:0] i_dat,
	input  wire [ 3:0] i_sel,
	output reg  [31:0] o_rdt,
	input  wire        i_we,
	input  wire        i_cyc,
	output reg         o_ack,
	output reg         o_int,

	// Output to GPIO drivers
	output wire [31:0] o_out,
	// Direction control to GPIO drivers
	output wire [31:0] o_dir,
	// Input from GPIO receivers
	input  wire [31:0] i_in
);

`define ADDR_DIR      6'h00
`define ADDR_DIRSET   6'h04 // reads DIR
`define ADDR_DIRCLR   6'h08 // reads DIR
`define ADDR_DIRTGL   6'h0C // reads DIR
`define ADDR_OUT      6'h10
`define ADDR_OUTSET   6'h14 // reads OUT
`define ADDR_OUTCLR   6'h18 // reads OUT
`define ADDR_OUTTGL   6'h1C // reads OUT
`define ADDR_IN       6'h20
`define ADDR_INTFLAGS 6'h24
`define ADDR_INTEN_R  6'h28
`define ADDR_INTEN_F  6'h2C

reg  [31:0] out      = 32'h00;
reg  [31:0] dir      = 32'h00;
reg  [31:0] intflags = 32'h00;
reg  [31:0] intlast  = 32'h00;
reg  [31:0] inten_r  = 32'h00;
reg  [31:0] inten_f  = 32'h00;

reg  [31:0] in_sync1   = 32'h0;
reg  [31:0] in_sync2   = 32'h0;
reg  [31:0] in         = 32'h0;

reg  [31:0] rise;
reg  [31:0] fall;

wire        access  = i_cyc && !o_ack;
wire        waccess = i_cyc && !o_ack && i_we;
wire        raccess = i_cyc && !o_ack && !i_we;
integer i;
genvar gi;

// o_ack
always @(posedge i_clk) begin
	o_ack <= i_cyc;
end

// IO drivers
assign o_out = out;
assign o_dir = dir;

// Input synchronizers
always @(posedge i_clk) begin
	if (i_rst) begin
		in_sync1 <= 32'h0;
		in_sync2 <= 32'h0;
		in       <= 32'h0;
	end else begin
		in_sync1 <= i_in;
		in_sync2 <= in_sync1;
		in       <= in_sync2;
	end
end

// Interrupt detectors
always @(posedge i_clk) begin
	rise <= ~in & in_sync2;
	fall <= in & ~in_sync2;
	o_int <= |intlast;
end

// Interrupts
reg [31:0] clear_req;
for (gi = 0; gi < 32; gi++)
always @(posedge i_clk) begin
	intlast[gi] <= intflags[gi];
	clear_req[gi] =
		waccess
		&& (i_adr == `ADDR_INTFLAGS)
		&& (i_dat[gi]);

	if (i_rst) begin
		intflags[gi] <= 1'b0;
	end else if (rise[gi] && inten_r[gi]) begin
		intflags[gi] <= 1'b1;
	end else if (fall[gi] && inten_f[gi]) begin
		intflags[gi] <= 1'b1;
	end else if (clear_req) begin
		intflags[gi] <= 1'b0;
	end
end

// Outputs
always @(posedge i_clk) begin
	if (i_rst) begin
		out <= 32'h00;
		dir <= 32'h00;
		inten_r <= 32'h00;
		inten_f <= 32'h00;
	end else if (waccess && (i_adr == `ADDR_DIR)) begin
		dir <= i_dat;
	end else if (waccess && (i_adr == `ADDR_DIRSET)) begin
		dir <= dir | i_dat;
	end else if (waccess && (i_adr == `ADDR_DIRCLR)) begin
		dir <= dir & ~i_dat;
	end else if (waccess && (i_adr == `ADDR_DIRTGL)) begin
		dir <= dir ^ i_dat;
	end else if (waccess && (i_adr == `ADDR_OUT)) begin
		out <= i_dat;
	end else if (waccess && (i_adr == `ADDR_OUTSET)) begin
		out <= out | i_dat;
	end else if (waccess && (i_adr == `ADDR_OUTCLR)) begin
		out <= out & ~i_dat;
	end else if (waccess && (i_adr == `ADDR_OUTTGL)) begin
		out <= out ^ i_dat;
	end else if (waccess && (i_adr == `ADDR_INTEN_R)) begin
		inten_r <= i_dat;
	end else if (waccess && (i_adr == `ADDR_INTEN_F)) begin
		inten_f <= i_dat;
	end
end

// Inputs
always @(posedge i_clk) begin
	if (raccess && i_adr >= `ADDR_DIR && i_adr < `ADDR_OUT) begin
		o_rdt <= dir;
	end else if (raccess && i_adr >= `ADDR_OUT && i_adr < `ADDR_IN) begin
		o_rdt <= out;
	end else if (raccess && (i_adr == `ADDR_IN)) begin
		o_rdt <= in;
	end else if (raccess && (i_adr == `ADDR_INTFLAGS)) begin
		o_rdt <= intlast;
	end else if (raccess && (i_adr == `ADDR_INTEN_R)) begin
		o_rdt <= inten_r;
	end else if (raccess && (i_adr == `ADDR_INTEN_F)) begin
		o_rdt <= inten_f;
	end else if (raccess) begin
		o_rdt <= 32'h00;
	end
end

endmodule
