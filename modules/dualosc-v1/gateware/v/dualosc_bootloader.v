module dualosc_bootloader (
	input clk,
	input i_rst,
	output [17:0] a,
	inout  [7:0]  d,
	output        nrd,
	output        nwr,
	output        stb0,
	output        stb1,

	// I2S interface
	output        sdoa,
	input         sdia,
	output        sdob,
	input         sdib,
	output        fs,
	output        bclk,
	output reg       codec_clk,

	// ADC interface
	output        adc_sclk,
	input         adc_cipo,
	output        adc_copi,
	output        adc_clk,
	output        adc_ncs,

	// RS-485 interface
	output        txd,
	output        txen,
	input         rxd,

	// DCV interface
	input         dcv,

	// Thermistor
	output        temp1,
	input         temp2,

	// Flash
	output        flash_copi,
	input         flash_cipo,
	output        flash_sck,
	output        flash_ss,

	// GPIOs
	output        wdi,
	inout         adc_ndrdy,
	inout         adc_nsync,
	inout         fp_scl,
	inout         fp_sda,
	inout         fp_nrst,
	inout         fp_nint,
	inout         ncsa,
	inout         ncsb,
	inout         nattn,
	inout         nirq
);

// System memory map
// All memory accesses are via an 8-bit bus, bridged to the 32-bit bus with a
// wb_adapter. This will perform four bus cycles for every access. The speed
// lost here is pretty negligible, since the bit-serial CPU runs at an
// instruction clock of 64 MHz/32 = 2 MHz whereas these 8-bit bus transactions
// are doable at the full 64 MHz.
//
// Internal - all single-cycle access
// ----------------------------------
// 0000 ---- aaaa aaaa aaaa  block RAM (4KB)
// 0001 ---- aaaa aaaa aaaa  GPIO
// 0010 ---- aaaa aaaa aaaa  UART
// 0011 ---- aaaa aaaa aaaa  SPI
//
// External - all equal wait states for 55ns SRAM
// -----------------------------------------------------
// 10aa aaaa aaaa aaaa aaaa  external peripheral 0 (MT093)
// 11aa aaaa aaaa aaaa aaaa  external peripheral 1 (SRAM)
//
// Only 20 address bits are actually used.
localparam ADDR_WIDTH = 20;

assign sdoa = 1'b0;
assign sdob = 1'b0;
assign fs   = 1'b0;
assign bclk = 1'b0;
assign adc_sclk = 1'b0;
assign adc_copi = 1'b0;
assign adc_clk  = 1'b0;
assign adc_ncs  = 1'b1;
assign txen = 1'b0;
assign adc_ndrdy = 1'bZ;
assign adc_nsync = 1'b1;
assign fp_scl = 1'bZ;
//assign fp_sda = 1'bZ;
//assign fp_nrst = 1'b0;
//assign fp_nint = 1'bZ;
assign ncsa = 1'b1;
assign ncsb = 1'b1;
assign nattn = 1'bZ;
assign nirq  = 1'bZ;

// SERV instruction bus
wire [31:0] ibus_adr;
wire        ibus_cyc;
wire [31:0] ibus_rdt;
wire        ibus_ack;

// SERV data bus
wire [31:0] dbus_adr;
wire [31:0] dbus_dat;
wire [3:0]  dbus_sel;
wire        dbus_we;
wire        dbus_cyc;
wire [31:0] dbus_rdt;
wire        dbus_ack;

// Combined bus
wire [ADDR_WIDTH-1:0] wb_adr;
wire [31:0] wb_dat;
wire [3:0]  wb_sel;
wire        wb_we;
wire        wb_cyc_req;
wire        wb_cyc;
reg  [31:0] wb_cpu_rdt = 32'h0;
reg         wb_cpu_ack = 1'b0;

// Control signals for internal peripherals
wire [31:0] bus_rdt_bram;
reg         bus_stb_bram;
wire        bus_ack_bram;
wire [31:0] bus_rdt_gpio;
reg         bus_stb_gpio;
wire        bus_ack_gpio;
wire [31:0] bus_rdt_uart;
reg         bus_stb_uart;
wire        bus_ack_uart;
wire [31:0] bus_rdt_spi;
reg         bus_stb_spi;
wire        bus_ack_spi;
wire [31:0] bus_rdt_ext;
reg         bus_stb_ext;
wire        bus_ack_ext;
reg         bus_undef_ack;

// 8-bit bus
wire [ADDR_WIDTH-1:0] bus8_adr;
wire [7:0]  bus8_dat;
wire        bus8_we;
wire        bus8_cyc;
wire [7:0]  bus8_rdt;
wire        bus8_ack;

`define RISCV_NOP 32'h00000013
`define RISCV_LOOP 32'h0000006F

initial begin
	codec_clk <= 1'b0;
end

always @(posedge clk) begin
	codec_clk <= !codec_clk;
end

reg reset = 1'b1;
reg [4:0] reset_ctdn = -1;

always @(posedge clk) begin
	if (i_rst) begin
		reset_ctdn <= -1;
		reset <= 1'b1;
	end
	else reset_ctdn <= reset_ctdn - 1;
	if (!i_rst && reset_ctdn == 0) reset <= 1'b0;
end
/*
serv_adapter serv_adapter(
	.i_clk(clk),
	.i_rst(reset),

	.i_ibus_adr(ibus_adr),
	.i_ibus_cyc(ibus_cyc),
	.o_ibus_rdt(ibus_rdt),
	.o_ibus_ack(ibus_ack),

	.i_dbus_adr(dbus_adr),
	.i_dbus_dat(dbus_dat),
	.i_dbus_sel(dbus_sel),
	.i_dbus_we(dbus_we),
	.i_dbus_cyc(dbus_cyc),
	.o_dbus_rdt(dbus_rdt),
	.o_dbus_ack(dbus_ack),

	.o_adr(bus8_adr),
	.o_dat(bus8_dat),
	.o_we(bus8_we),
	.o_cyc(bus8_cyc),
	.i_rdt(bus8_rdt),
	.i_ack(bus8_ack)
);
*/

always @(posedge clk) bus_undef_ack <= bus8_cyc;

// Pipelining the address decode and crossbar gets it off the critical path,
// though it adds two wait states. That's ok - that's just 1/16 of an
// instruction clock.
reg wb_cyc_dly;
reg bus_stb_bad;
assign wb_cyc = wb_cyc_dly & wb_cyc_req;
always @(posedge clk) begin
	// Address decoder
	bus_stb_bram <= wb_adr[19:16] == 4'h0;
	bus_stb_gpio <= wb_adr[19:16] == 4'h1;
	bus_stb_uart <= wb_adr[19:16] == 4'h2;
	bus_stb_spi  <= wb_adr[19:16] == 4'h3;
	bus_stb_ext  <= wb_adr[19]    == 1'b1;
	bus_stb_bad  <= wb_adr[19:16] >= 4'h4 && wb_adr[19:16] < 4'h8;

	wb_cyc_dly <= wb_cyc_req;

	wb_cpu_rdt <=
		bus_stb_bram ? bus_rdt_bram :
		bus_stb_ext  ? bus_rdt_ext :
		bus_stb_uart ? bus_rdt_uart :
		bus_stb_spi  ? bus_rdt_spi :
		bus_stb_gpio ? bus_rdt_gpio :
		32'hDEADBEEF;

	wb_cpu_ack <= |{
		bus_stb_bram && bus_ack_bram,
		bus_stb_ext  && bus_ack_ext,
		bus_stb_uart && bus_ack_uart,
		bus_stb_gpio && bus_ack_gpio,
		bus_stb_spi  && bus_ack_spi,
		bus_stb_bad  && bus_undef_ack
	};
end

ram32 #(.ADDR_WIDTH(13), .INIT_FILE("fw/bootloader.mem")) ram(
	.i_clk(clk),
	.i_rst(reset),
	.i_adr(wb_adr[12:0]),
	.i_dat(wb_dat),
	.i_sel(wb_sel),
	.o_rdt(bus_rdt_bram),
	.i_we(wb_we),
	.i_cyc(wb_cyc && bus_stb_bram),
	.o_ack(bus_ack_bram)
);

uart_top uart(
	.wb_clk_i(clk),
	.wb_rst_i(reset),
	.wb_adr_i(wb_adr[4:2]),
	.wb_dat_i(wb_dat[7:0]),
	.wb_dat_o(bus_rdt_uart[7:0]),
	.wb_we_i(wb_we),
	.wb_stb_i(bus_stb_uart && wb_sel[0]),
	.wb_cyc_i(wb_cyc),
	.wb_ack_o(bus_ack_uart),
	.wb_sel_i(4'hF),
	.stx_pad_o(txd),
	.srx_pad_i(rxd),
	.cts_pad_i(1'b1),
	.dsr_pad_i(1'b1),
	.ri_pad_i(1'b0),
	.dcd_pad_i(1'b0)
);
assign bus_rdt_uart[31:8] = 24'h0;

wire [7:0] spi_ss;
wire spi_sclk, spi_mosi, spi_miso;
wire spi_int;
simple_spi_top spi(
	.clk_i(clk),
	.rst_i(!reset), // ugh it's active low
	.cyc_i(wb_cyc),
	.stb_i(bus_stb_spi),
	.adr_i(wb_adr[3:2]),
	.we_i(wb_we),
	.dat_i(wb_dat[7:0]),
	.dat_o(bus_rdt_spi[7:0]),
	.ack_o(bus_ack_spi),
	.inta_o(spi_int),
	.sck_o(spi_sclk),
	.mosi_o(spi_mosi),
	.miso_i(spi_miso)
);
assign bus_rdt_spi[31:8] = 24'h0;
assign flash_copi = spi_mosi;
assign flash_sck  = spi_sclk;
assign spi_miso   = flash_cipo;
assign flash_ss   = 1'b0; // TODO - gpio

wire [31:0] gpio_out;
wire [31:0] gpio_dir;
wire [31:0] gpio_in;
gpio gpio0(
	.i_clk(clk),
	.i_rst(reset),
	.i_adr(wb_adr[5:0]),
	.i_dat(wb_dat),
	.o_rdt(bus_rdt_gpio),
	.i_we(wb_we),
	.i_sel(wb_sel),
	.i_cyc(wb_cyc && bus_stb_gpio),
	.o_ack(bus_ack_gpio),
	.o_out(gpio_out),
	.o_dir(gpio_dir),
	.i_in(gpio_in)
);
// GPIO 0 = WDI
assign wdi = gpio_dir[0] ? gpio_out[0] : 1'bZ; assign gpio_in[0] = wdi;
// GPIO 1 = WDI feedback for interrupt test
assign gpio_in[1] = wdi;
// Unused
assign gpio_in[31:2] = 0;

serv_rf_top serv(
	.clk(clk),
	.i_rst(reset),
	.i_timer_irq(1'b0),

	.o_ibus_adr(ibus_adr),
	.o_ibus_cyc(ibus_cyc),
	.i_ibus_rdt(ibus_rdt),
	.i_ibus_ack(ibus_ack),

	.o_dbus_adr(dbus_adr),
	.o_dbus_dat(dbus_dat),
	.o_dbus_sel(dbus_sel),
	.o_dbus_we(dbus_we),
	.o_dbus_cyc(dbus_cyc),
	.i_dbus_rdt(dbus_rdt),
	.i_dbus_ack(dbus_ack),

	.i_ext_rd(32'hDEADBEEF),
	.i_ext_ready(1'b1)
);

wire dirty_ibus_ack, dirty_dbus_ack;
wire [31:0] full_wb_adr;
servile_arbiter cpu_arb(
	.i_wb_cpu_dbus_adr(dbus_adr),
	.i_wb_cpu_dbus_dat(dbus_dat),
	.i_wb_cpu_dbus_sel(dbus_sel),
	.i_wb_cpu_dbus_we(dbus_we),
	.i_wb_cpu_dbus_stb(dbus_cyc),
	.o_wb_cpu_dbus_rdt(dbus_rdt),
	.o_wb_cpu_dbus_ack(dirty_dbus_ack),
	.i_wb_cpu_ibus_adr(ibus_adr),
	.i_wb_cpu_ibus_stb(ibus_cyc),
	.o_wb_cpu_ibus_rdt(ibus_rdt),
	.o_wb_cpu_ibus_ack(dirty_ibus_ack),
	.o_wb_mem_adr(full_wb_adr),
	.o_wb_mem_dat(wb_dat),
	//.o_wb_mem_sel(wb_sel),
	.o_wb_mem_we(wb_we),
	.o_wb_mem_stb(wb_cyc_req),
	.i_wb_mem_rdt(wb_cpu_rdt),
	.i_wb_mem_ack(wb_cpu_ack)
);
// lol what is it with serv and dirty wishbone
// it's not hard to clean this up
assign wb_sel = ibus_cyc ? 4'hF : dbus_sel;
assign dbus_ack = dbus_cyc && dirty_dbus_ack;
assign ibus_ack = ibus_cyc && dirty_ibus_ack;
assign wb_adr = full_wb_adr[ADDR_WIDTH-1:0];

// Bus resizer serves the external 8-bit interface
bus_resizer #(.ADDR_WIDTH(ADDR_WIDTH)) resizer(
	.i_clk(clk),
	.i_rst(reset),

	.i_32b_adr(wb_adr),
	.i_32b_dat(wb_dat),
	.i_32b_sel(wb_sel),
	.i_32b_we (wb_we),
	.i_32b_cyc(wb_cyc & bus_stb_ext),
	.o_32b_rdt(bus_rdt_ext),
	.o_32b_ack(bus_ack_ext),

	.o_8b_adr(bus8_adr),
	.o_8b_dat(bus8_dat),
	.o_8b_we (bus8_we),
	.o_8b_cyc(bus8_cyc),
	.i_8b_rdt(bus8_rdt),
	.i_8b_ack(bus8_ack)
);

wire ext_stb;
assign a = bus8_adr[17:0];
assign stb0 = !bus8_adr[18] && ext_stb;
assign stb1 =  bus8_adr[18] && ext_stb;

ext_bus #(
	.ADDR_TO_RD_READY(4), // 4 for 55ns, 5 for 70ns (at 64 MHz)
	.RD_HOLD(1),
	.WR_SETUP(1),
	.WR_STROBE(3),        // 3 for 55ns, 4 for 70ns (at 64 MHz)
	.WR_HOLD(1)
) ext_bus(
	.i_clk(clk),
	.i_rst(reset),
	.i_cyc(bus8_cyc),
	.i_we(bus8_we),
	.i_dat(bus8_dat),
	.o_rdt(bus8_rdt),
	.o_ack(bus8_ack),
	.io_d(d),
	.o_nrd(nrd),
	.o_nwr(nwr),
	.o_cs(ext_stb)
);

assign fp_nint = txd;
assign fp_nrst = i_rst;
//

//assign fp_sda  = bus8_adr[18];

// About 680 Hz at room temp
reg temp_drive;
always @(posedge clk) begin
	temp_drive <= !temp2;
end
assign temp1=temp_drive;

endmodule
