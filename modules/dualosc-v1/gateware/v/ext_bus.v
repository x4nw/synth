module ext_bus #(
	// Number of cycles from asserting addr/nrd/stb to latching data
	// (max(tAA, tDOE) in datasheet)
	parameter ADDR_TO_RD_READY = 1,
	// Number of cycles from latching data to deasserting addr/nrd/stb
	parameter RD_HOLD = 1,
	// Number of cycles from asserting addr/data/nwe to asserting stb
	// (tSA)
	parameter WR_SETUP = 1,
	// Number of cycles to hold stb
	// (tSCS1)
	parameter WR_STROBE = 1,
	// Number of cycles from asserting addr/data/nwe to deasserting
	// addr/data
	// (tWC - tSCS1)
	parameter WR_HOLD = 1
)
(
	input  wire        i_clk,
	input  wire        i_rst,
	input  wire        i_cyc,
	input  wire        i_we,
	input  wire [ 7:0] i_dat,
	output reg  [ 7:0] o_rdt,
	output reg         o_ack,

	inout  wire [ 7:0] io_d,
	output wire        o_nrd,
	output wire        o_nwr,
	output reg         o_cs
);

localparam rd_top = ADDR_TO_RD_READY + RD_HOLD + 2;
localparam wr_top = WR_HOLD + WR_STROBE + WR_SETUP + 2;
localparam max_count = rd_top > wr_top ? rd_top : wr_top;
localparam wcount = $clog2(max_count + 1);

// Read cycle:
//                |<- RD_HOLD + 2
//                |   |<- 2
// i_cyc  ___/^^^^^^^^^^\_
// o_ack  ____________/^\_
// count     L    .   .
// o_cs   ___/^^^^\______
// o_rdt  --------<=====>

// Write cycle:
//                |<- WR_STROBE + WR_HOLD + 2
//                |   |<- WR_HOLD + 2
//                |   |   |<- 2
// i_cyc  ___/^^^^^^^^^^^^^^\_
// o_ack  ________________/^\_
// count     L    .   .   .
// o_cs   ________/^^^\_______
// io_d   ---<============>---

reg [wcount-1:0] count = 0;
reg data_oe = 1'b0;

assign io_d  = data_oe ? i_dat : 8'hZZ;
assign o_nrd = i_we;
assign o_nwr = !i_we;

always @(posedge i_clk) begin
	if (i_rst) count <= 0;
	else if (count > 0) count <= count - 1;
	else if (i_cyc) count <= i_we ? wr_top : rd_top;
end

always @(posedge i_clk) begin
	if (i_rst || (!i_cyc && count == 0)) begin
		data_oe <= 1'b0;
		o_cs <= 1'b0;
		o_ack <= 1'b0;
	end if (i_we) begin
		if (count == wr_top) begin
			data_oe <= 1'b1;
		end else if (count == WR_STROBE + WR_HOLD + 2) begin
			o_cs <= 1'b1;
		end else if (count == WR_HOLD + 2) begin
			o_cs <= 1'b0;
		end else if (count == 2) begin
			o_ack <= 1'b1;
		end else if (count == 1) begin
			o_ack <= 1'b0;
		end
	end else begin
		if (count == rd_top) begin
			o_cs <= 1'b1;
		end else if (count == RD_HOLD + 2) begin
			o_rdt <= io_d;
			o_cs <= 1'b0;
		end else if (count == 2) begin
			o_ack <= 1'b1;
		end else if (count == 1) begin
			o_ack <= 1'b0;
		end
	end
end

endmodule
