module top (
	input clk16,
	output [17:0] a,
	inout  [7:0]  d,
	output        nrd,
	output        nwr,
	output        stb0,
	output        stb1,

	// I2S interface
	output        sdoa,
	input         sdia,
	output        sdob,
	input         sdib,
	output        fs,
	output        bclk,
	output        codec_clk,

	// ADC interface
	output        adc_sclk,
	input         adc_cipo,
	output        adc_copi,
	output        adc_clk,
	output        adc_ncs,

	// RS-485 interface
	output        txd,
	output        txen,
	input         rxd,

	// DCV interface
	input         dcv,

	// Thermistor
	output        temp1,
	input         temp2,

	// Flash
	output        flash_copi,
	input         flash_cipo,
	output        flash_sck,
	output        flash_ss,

	// GPIOs
	inout         wdi,
	inout         adc_ndrdy,
	inout         adc_nsync,
	inout         fp_scl,
	inout         fp_sda,
	inout         fp_nrst,
	inout         fp_nint,
	inout         ncsa,
	inout         ncsb,
	inout         nattn,
	inout         nirq,
);

wire pll_lock;
wire clk;

SB_PLL40_CORE #(
	.DIVF(7'd63), // Fvco = 512 MHz (533-1066 allowed, we're running a touch slow)
	.DIVR(4'd3),
	.DIVQ(3'd2),
	.FILTER_RANGE(3'b100),
	.FEEDBACK_PATH("SIMPLE"),
	.DELAY_ADJUSTMENT_MODE_FEEDBACK("FIXED"),
	.FDA_FEEDBACK(4'b0),
	.DELAY_ADJUSTMENT_MODE_RELATIVE("FIXED"),
	.FDA_RELATIVE(4'b0),
	.SHIFTREG_DIV_MODE(2'b0),
	.PLLOUT_SELECT("GENCLK"),
	.ENABLE_ICEGATE(1'b0)
) pll
(
	.RESETB(1'b1),
	.BYPASS(1'b0),
	.REFERENCECLK(clk16),
	.PLLOUTCORE(clk),
	.LOCK(pll_lock)
);

dualosc_bootloader dualosc_bootloader(
	.clk(clk),
	.i_rst(!pll_lock),
	.a(a),
	.d(d),
	.nrd(nrd),
	.nwr(nwr),
	.stb0(stb0),
	.stb1(stb1),
	.sdoa(sdoa),
	.sdia(sdia),
	.sdob(sdob),
	.sdib(sdib),
	.fs(fs),
	.bclk(bclk),
	.codec_clk(codec_clk),
	.adc_sclk(adc_sclk),
	.adc_cipo(adc_cipo),
	.adc_copi(adc_copi),
	.adc_clk(adc_clk),
	.adc_ncs(adc_ncs),
	.txd(txd),
	.txen(txen),
	.rxd(rxd),
	.dcv(dcv),
	.temp1(temp1),
	.temp2(temp2),
	.flash_copi(flash_copi),
	.flash_cipo(flash_cipo),
	.flash_sck(flash_sck),
	.flash_ss(flash_ss),
	.wdi(wdi),
	.adc_ndrdy(adc_ndrdy),
	.adc_nsync(adc_nsync),
	.fp_scl(fp_scl),
	.fp_sda(fp_sda),
	.fp_nrst(fp_nrst),
	.fp_nint(fp_nint),
	.ncsa(ncsa),
	.ncsb(ncsb),
	.nattn(nattn),
	.nirq(nirq)
);

endmodule
