// Adapt SERV's dual 32-bit buses into a single 8-bit bus.
module serv_adapter #(
	parameter ADDR_WIDTH = 20
)
(
	input wire i_clk,
	input wire i_rst,

	// -- SERV instruction bus --
	input  wire [31:0] i_ibus_adr,
	input  wire        i_ibus_cyc,
	output wire [31:0] o_ibus_rdt,
	output wire        o_ibus_ack,

	// -- SERV data bus --
	input  wire [31:0] i_dbus_adr,
	input  wire [31:0] i_dbus_dat,
	input  wire [ 3:0] i_dbus_sel,
	input  wire        i_dbus_we,
	input  wire        i_dbus_cyc,
	output wire [31:0] o_dbus_rdt,
	output wire        o_dbus_ack,

	// -- System 8-bit bus
	output wire [ADDR_WIDTH-1:0] o_adr,
	output reg  [           7:0] o_dat,
	output wire                  o_we,
	output wire                  o_cyc,
	input  wire [           7:0] i_rdt,
	input  wire                  i_ack
);

localparam S_RW0  = 3'b100;
localparam S_RW1  = 3'b101;
localparam S_RW2  = 3'b110;
localparam S_RW3  = 3'b111;
localparam S_IDLE = 3'b000;

reg  [2:0]  state = S_IDLE;
reg  [23:0] data  = 24'h0;

wire [1:0] bytesel = state[1:0];
wire       run     = state[2];

// Output data mux
always @(bytesel, i_dbus_dat) case (bytesel)
	2'h0: o_dat = i_dbus_dat[ 7: 0];
	2'h1: o_dat = i_dbus_dat[15: 8];
	2'h2: o_dat = i_dbus_dat[23:16];
	2'h3: o_dat = i_dbus_dat[31:24];
endcase

// Input data latches
always @(posedge i_clk) begin
	if (bytesel == 2'h0) data[ 7: 0] <= i_rdt;
	if (bytesel == 2'h1) data[15: 8] <= i_rdt;
	if (bytesel == 2'h2) data[23:16] <= i_rdt;
end
assign o_dbus_rdt = {i_rdt, data};
assign o_ibus_rdt = {i_rdt, data};

// Byte selects
wire sel = i_ibus_cyc || (i_dbus_cyc && !i_dbus_we) || i_dbus_sel[bytesel];
wire proceed = i_ack || !sel;

// Output control signals
reg    [31:0] base_addr;
assign o_adr = {base_addr[ADDR_WIDTH-1:2], bytesel};
assign o_we  = i_dbus_cyc && i_dbus_we;
assign o_cyc = run && sel;
assign o_ibus_ack = i_ibus_cyc && proceed && (state == S_RW3);
assign o_dbus_ack = i_dbus_cyc && proceed && (state == S_RW3);

always @* begin
	if (i_ibus_cyc) base_addr = i_ibus_adr;
	else            base_addr = i_dbus_adr;
end

reg [2:0] next_state;

always @* begin
	next_state = state;
	case (state)
		S_IDLE: if (i_ibus_cyc || i_dbus_cyc) next_state = S_RW0;
		S_RW0: if (proceed) next_state = S_RW1;
		S_RW1: if (proceed) next_state = S_RW2;
		S_RW2: if (proceed) next_state = S_RW3;
		S_RW3: if (proceed) next_state = S_IDLE;
	endcase
	if (i_rst) next_state = S_IDLE;
end

always @(posedge i_clk) state <= next_state;

endmodule
