// intellectual property is bullshit bgdc

#ifndef SSPI_H
#define SSPI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// Register definitions for the opencores Simple SPI wishbone core based on
// 68HC11, mounted on a 32-bit bus
__attribute__((packed))
struct sspi
{
	uint8_t SPCR;
	uint8_t _pad0[3];
	uint8_t SPSR;
	uint8_t _pad1[3];
	uint8_t SPDR;
	uint8_t _pad2[3];
	uint8_t SPER;
	uint8_t _pad3[3];
};

#define SSPI_SPCR_SPIE 0x80 // Serial Peripheral Interrupt Enable
#define SSPI_SPCR_SPE  0x40 // Serial Peripheral Enable
#define SSPI_SPCR_MSTR 0x10 // Master Mode Select
#define SSPI_SPCR_CPOL 0x08 // Clock Polarity
#define SSPI_SPCR_CPHA 0x04 // Clock Phase
#define SSPI_SPCR_SPR_bp 0  // SPI Clock Rate Select
#define SSPI_SPCR_SPR_gm (3 << SSPI_SPCR_SPR_bp)

#define SSPI_SPSR_SPIF    0x80 // Serial Peripheral Interrupt Flag
#define SSPI_SPSR_WCOL    0x40 // Write Collision
#define SSPI_SPSR_WFFULL  0x08 // Write FIFO Full
#define SSPI_SPSR_WFEMPTY 0x04 // Write FIFO Empty
#define SSPI_SPSR_RFFULL  0x02 // Read FIFO Full
#define SSPI_SPSR_RFEMPTY 0x01 // Read FIFO Empty

#define SSPI_SPER_ICNT_bp   6 // Interrupt Count - determines transfer block size
#define SSPI_SPER_ICNT_gm   (3 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_1_gc (0 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_2_gc (1 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_3_gc (2 << SSPI_SPER_ICNT_bp)
#define SSPI_SPER_ICNT_4_gc (3 << SSPI_SPER_ICNT_bp)

#define SSPI_SPER_ESPR_bp 0 // Extended SPI Clock Rate Select
#define SSPI_SPER_ESPR_gm (3 << SSPI_SPER_ESPR_bp)

// SPI clock rates. These get unpacked into both SPER.ESPR and SPCR.SPR
#define SSPI_CDIV_2    0x0
#define SSPI_CDIV_4    0x1
#define SSPI_CDIV_16   0x2
#define SSPI_CDIV_32   0x3
#define SSPI_CDIV_8    0x4
#define SSPI_CDIV_64   0x5
#define SSPI_CDIV_128  0x6
#define SSPI_CDIV_256  0x7
#define SSPI_CDIV_512  0x8
#define SSPI_CDIV_1024 0x9
#define SSPI_CDIV_2048 0xA
#define SSPI_CDIV_4096 0xB
#define SSPI_SPCR_SPR_CDIV(x)  (((x) & 0x3) << SSPI_SPCR_SPR_bp)
#define SSPI_SPER_ESPR_CDIV(x) ((((x) & 0xC) >> 2) << SSPI_SPER_ESPR_bp)

#ifdef __cplusplus
}
#endif

#endif // !defined(SSPI_H)
