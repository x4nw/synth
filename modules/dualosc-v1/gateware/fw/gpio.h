// intellectual property is bullshit bgdc

#ifndef GPIO_H
#define GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

struct gpio
{
	uint32_t DIR;
	uint32_t DIRSET;
	uint32_t DIRCLR;
	uint32_t DIRTGL;
	uint32_t OUT;
	uint32_t OUTSET;
	uint32_t OUTCLR;
	uint32_t OUTTGL;
	uint32_t const IN;
	uint32_t INTFLAGS;
	uint32_t INTEN_R;
	uint32_t INTEN_F;
};

#ifdef __cplusplus
}
#endif

#endif // !defined(GPIO_H)
