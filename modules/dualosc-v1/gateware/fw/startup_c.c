#include <inttypes.h>

int main(void);

void _start_c(void)
{
	extern uint32_t volatile __bss_start, __bss_end;
	for (uint32_t volatile * i = &__bss_start; i < &__bss_end; i++)
		*i = 0;
	(void) main();
}
