#include <inttypes.h>
#include <stddef.h>

#include "uart16550.h"
#include "sspi.h"
#include "gpio.h"

#define F_PER 64000000uL
#define F_BAUD 115200uL


#define UART ((struct uart16550_32 volatile *) 0x20000)
#define SSPI ((struct sspi volatile *) 0x30000)
#define GPIO ((struct gpio volatile *) 0x10000)
#define EXT  ((uint8_t volatile *)  0x80000)

static void send(char c);

int main(void)
{
	static const char text[] = "Hello, world!";

	uint16_t divisor = F_PER / (16 * F_BAUD);
	uint8_t const lcr =
		UART_LCR_WLS_8BIT_gc
		| UART_LCR_STB_2_gc
		| UART_LCR_PARITY_NONE_gc;
	UART->LCR = lcr | UART_LCR_DLA;
	UART->DIVISOR_MSB = divisor >> 8;
	UART->DIVISOR_LSB = divisor & 0xFF;
	UART->LCR = lcr;
	UART->FCR = UART_FCR_FIFOEN;
	UART->MCR = 0;
	GPIO->DIR = 1;
	GPIO->INTEN_R = 2;
	(void) EXT[0x420];
	EXT[0x1337] = 0x69;

	uint8_t const cdiv = SSPI_CDIV_4;
	SSPI->SPER = SSPI_SPER_ESPR_CDIV(cdiv);
	SSPI->SPCR = SSPI_SPCR_SPE | SSPI_SPCR_SPR_CDIV(cdiv);
	SSPI->SPDR = 0x69;

	for (;;)
	{
		for (size_t i = 0; text[i]; i++)
			send(text[i]);
		for (volatile uint32_t i = 128; i; i--);
	}
}

//__attribute__((noinline))
static void send(char c)
{
	for (;;)
	{
		uint8_t lsr = UART->LSR;
		if (lsr & UART_LSR_DR)
		{
			(void) UART->RBR;
		}
		if (lsr & UART_LSR_THRE)
		{
			GPIO->OUTTGL = 1;
			UART->THR = (uint8_t) c;
			return;
		}
	}
}
