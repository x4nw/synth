# Backplane interface

## Electrical

| Pin    | Name              | SCM? | Description |
|:------:|-------------------|:----:|-------------|
| B1, A1 | +12V              |  Y   | +12V supply. Current TBD. |
| B2, A2 | -12V              |  Y   | -12V supply. Current TBD. |
| B3-4, A3-4 | +5V           |  Y   | +5V supply. Current TBD. |
| B5, 13, 15, 17, 19, 21-24, 27, 28, 31 | Y | GND | GND |
| A5-7, 14, 16, 18, 20, 22, 25, 26, 29, 30 | Y | GND | GND |
| B7     | nRSTREQ           |  Out | Reset request, active low |
| B8     | nATTN#            |  N   | Attention/geographic address. Active low. |
| B9     | nIRQ#             |  N   | Interrupt request. Active low. |
| B32    | nDETECT#          |  N   | Card detect. Active low. |
| B10,11 | RS485 A,B         |  Y   | RS-485 interface. |
| B12    | DCV               |  N   | Digital control voltage. |
| A8     | BP\_CLK           | Only | Backplane control clock. |
| A9     | BP\_DATA          | Only | Backplane control data. |
| A10    | BP\_nCS           | Only | Backplane chip select. |
| A11    | DCV\_OUT          | Only | Digital control voltage out. |
| A12    | nDETECT           | Only | Detect signal input to SCM. |
| A13    | nIRQ              | Only | IRQ signal to SCM. |
| A23,24 | +3V3              | Only | +3.3V supply out from SCM. |
| B14    | Bus1              |  Y   | Routing bus signal 1. |
| A15    | Bus2              |  Y   | Routing bus signal 2. |
| B16    | Bus3              |  Y   | Routing bus signal 3. |
| A17    | Bus4              |  Y   | Routing bus signal 4. |
| B18    | Bus5              |  Y   | Routing bus signal 5. |
| A19    | Bus6              |  Y   | Routing bus signal 6. |
| B20    | Bus7              |  Y   | Routing bus signal 7. |
| A21    | Bus8              |  Y   | Routing bus signal 8. |
| A23,24 | Env+, Env-        |  N   | Envelope in. |
| B25,26 | OutA+, OutA-      |  N   | First output. |
| A27,28 | OutB+, OutB-      |  N   | Second output. |
| B29,30 | InA+, InA-        |  N   | First input. |
| A31,32 | InB+, InB-        |  N   | Second input. |

### nRSTREQ

This signal is shared between all boards in the system. It is driven low by
the SCM to reset all other boards, or otherwise undriven. It may be pulled high
on each card by no stronger than 100k ohms to 3.3V.

Currently, non-SCM boards are not allowed to drive this line low.

### nATTN\#

Used for geographical addressing: when the special geographical address is
sent in an RS-485 command, devices with this line asserted should interpret
it. Should be electrically connected on each board such that the RS-485
transmitter is forced disabled when asserted; it may be used to remove a
malfunctioning board from the bus if necessary.

### nIRQ\# and nIRQ

Used by boards to request service from the SCM. The backplane logic circuit
combines all asserted nIRQ\# lines, gated by the IRQMASK register, into the
single nIRQ line sent back to the SCM.

### nDETECT\# and nDETECT

Detects the presence of a board. Each board should tie its nDETECT\# to ground
(the SCM should not, unless it also has a non-SCM circuit combined onto one
card). The nDETECT line selector on the backplane chooses which one to feed
to nDETECT, which should be pulled high on the SCM.

### DCV and DCV\_OUT

DCV provides the "digital control voltage", a MIDI-like UART interface used to
transmit pitch values perfectly instead of using an analog control voltage.
The interface is distributed to all backplanes from DCV\_OUT on the SCM, and
then selectively routed to the boards on each backplane if selected by the
control board.

Interface specifications are: 3.3V CMOS UART, 31250, 8N1; each pitch value is
transmitted as three bytes:

```
1 x x x x x x x    0 x x x x x x x    0 x x x x x x x

xxxxxxxxxxxxxxxxxxxxx = Pitch, encoded as MIDI note number * 2**14 + pitch bend
```

### Bus signals

Analog bus signals range from 0 to 3.3V. When encoding pitch, the convention
is 1/3 V per octave. When encoding bipolar values, center is represented by
1.65V (an offset calibration command should cause devices to latch a center
reading).

Digital signals use TTL input level: V<sub>IL</sub> <= 0.8V and
V<sub>IH</sub> >= 2.0V. They may be driven by any source compatible with these
levels, up to 3.3V maximum and 0V minimum. Receiver hysteresis is recommended
but not required.

Signals must be driven from a 620-700 ohm impedance (intention is a 620 ohm
resistor plus the series resistance of an analog switch), allowing signals to
be combined by driving multiple onto the bus at once. An exception is permitted
for digital bus signals, which may be driven from a simple current limited
source providing up to 5mA.

### Differential signals

For improved signal performance, the primary signal path is differential. Each
half of the differential signal follows the same specifications as the normal
bus signals. A signal and its inverse (inverted around 1.65V) should be
transmitted together, and subtracted at the receiver.

## RS-485 interface

Initializes at 115200 baud, 8N1. Commands are encoded in hexadecimal using
punctuation as control characters:

```
<addr> ":" <command> <argument> "\r"
```

Addresses are always 8 bits (two hexadecimal characters), and commands are
always 16 bits (four hexadecimal characters).

A break condition for at least 100ms should reset all interface settings
(currently just baud rate and the command parser state machine).

### Reserved addresses

00 chooses geographical addressing with reply. A device should respond to this
address if and only if its nATTN\# input is asserted. Because this signal
forces the device's RS-485 transmitter to deactivate, it should wait for the
signal to deassert before trying to respond.

01 chooses geographical addressing without reply. Interpreted as for 00, but
devices should suppress response as for broadcast (FF). Used to send a command
to multiple, but not all, devices.

FE denotes a reply. All devices except the SCM should ignore this address.

FF is the broadcast address. All devices should interpret commands sent to this
address, and none should respond to them (to avoid bus contention).

### Commands

| Command | Name           | Format            | Description |
|---------|----------------|-------------------|-------------|
| 0000    | GEN\_PING      | void -> ack       | Request an empty response |
| 0001    | GEN\_ADDR      | int -> ack        | Assign address to the selected device |
| 0002    | GEN\_BAUDCHK   | int -> bool       | Check if the requested baud rate is supported |
| 0003    | GEN\_BAUDSET   | int -> ack        | Set baud rate |
| 0010    | GEN\_DSC\_READ | int, int -> data  | Descriptor table: read arg2 bytes at arg1 |
| 00FE    | GEN\_ERROR     | ... -> int, str   | "Command" carrying an error response |
| 00FF    | GEN\_RESPONSE  | ... -> n/a        | "Command" carrying a response |
| 0100    | BTL\_READ      | int, int -> data  | Bootloader: read arg2 bytes at arg1 |
| 0101    | BTL\_WRITE     | int, data -> ack  | Bootloader: write arg2 to arg1 |
| 0102    | BTL\_ERASE     | int, int -> ack   | Bootloader: erase arg2 bytes at arg1 |
| 0103    | BTL\_BOOT      | void -> ack       | Bootloader: boot system |
| 0200    | SET\_READ      | int, int -> iarr  | Settings: read arg2 settings from number arg1 |
| 0201    | SET\_SET       | int, iarr -> ack  | Settings: write arg2 starting at number arg1 |
| 0202    | SET\_READSTR   | int -> str        | Settings: read string setting from number arg1 |
| 0203    | SET\_SETSTR    | int, str -> ack   | Settings: write string setting arg2 to number arg1 |

Arguments are packed in the order listed in the "format" column into the
argument field. Only one variable-length argument is supported per command and
must be in the final position. Types are:

| Type | Bytes  | Descrption |
|------|-------:|------------|
| void |      0 | Nothing    |
| ack  |      1 | Same as `bool`. True = ack, false = nack. Indicates success/failure |
| bool |      1 | Logic value. Zero is false, nonzero is true. |
| int  |      4 | Signed 32-bit integer. Transmitted little-endian. |
| data |  1 + n | Block of arbitrary data. First byte is length. |
| str  |  1 + n | Text string. Same format as `data`, assumed to encode UTF-8 string |
| iarr | 1 + 4n | Array of integers. First byte is length in integers (not bytes), data block is encoded as for `int` |

### Bootloader mode

Bootloader mode is entered by resetting the system (nRSTREQ) with nATTN\#
asserted. The same RS-485 command format is used, but only the `GEN_*` and
`BTL_*` commands need to be supported. The system will never be placed
partially into bootloader mode; either all modules or none will be.

Bootloader commands are basic, and requirements (including valid addresses
and maximum per-command length) may be set in a module-dependent manner, with
the one requirement that a bootloader cannot allow overwriting of itself
(a secondary bootloader block may be provided to allow bootloader upgrades with
a fallback). The build toolchain for each module is responsible for producing
a sequence of valid commands, which will be replayed by the SCM when required
(file format to be determined).

Bootloader commands should NOT be supported in non-bootloader mode.

### Descriptor format

The descriptor begins with `struct descriptor_root` as defined below.
Conventionally, all descriptor structs start with a version number, and
subsequent versions should append fields. Indices point to either additional
structs or to strings, by offset and length.

```

struct descriptor_root {
    uint16_t     version;
    char         module_short_id[16]; // ASCII module ID
    char         fw_version[16];      // ASCII firmware version
    struct index module_name;         // Points to a UTF-8 module name
    struct index settings;            // Points to struct setting_descriptor[]
};

struct setting_descriptor {
    char         id[16];              // ASCII ident for this setting
    struct index name;                // Short name for on-screen display
    struct index help;                // Help text for on-screen display
    enum stype   stype;               // Type of the setting
    int32_t      minimum;             // Minimum permitted value
    int32_t      maximum;             // Maximum permitted value
    int32_t      precision;           // Suggested precision (not always used, see below)
};
```

Companion types:

```
struct index {
    uint16_t offset;
    uint16_t length;
};

enum stype {
    STYPE_INT        = 0, // Just a basic integer
    STYPE_PERCENT    = 1, // Percentage 0...100, editable with arbitrary precision
                          // (internally, use 0 to 0x7FFFFFFF as range)
    STYPE_SPERCENT   = 2, // Signed percentage -100...100, arbitrary precision
                          // (internally, use -0x7FFFFFFF to +0x7FFFFFFF as range)
    STYPE_TIME_US    = 3, // Time in microseconds
    STYPE_BUS        = 4, // A single bus connection. Implies range 1...8
    STYPE_BUSBITS    = 5, // A bitmask of bus connections,
                          // bus n=1...8 corresponds to 1 << (n - 1)
    STYPE_MIDI       = 6, // MIDI note number. Display as e.g. Ab4
    STYPE_MIDI_TUNED = 7, // MIDI note number with 14-bit signed detune.
                          // Conventionally, display detune in cents, -50 to +50:
                          //   Ab4 + 32c
    STYPE_DETUNE     = 8, // Signed detune. Same precision as STYPE_MIDI_TUNED.
                          // Conventionally, display detune in halfsteps plus
                          // cents: 3 halfsteps and 50 cents is +3.50 and
                          // encoded as 0xD000.
};
```

A few types have unusual conventions for the range fields in the descriptor:

- `STYPE_BUS`, `STYPE_BUSBITS`: "maximum" holds a bitmask of bus lines
  permitted to be selected for this signal. In addition to the eight bus line
  bits, the following bits may be given:
  - 0x0100000: InA
  - 0x0200000: InB
  - 0x0400000: Env
  - 0x0800000: OutA
  - 0x1000000: OutB
- `STYPE_TIME_US`: "precision" holds the recommended increment for this setting
  (e.g. 1000 means this field is editable in milliseconds).
