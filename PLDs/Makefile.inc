# User options
RUN_SIMULATION ?= 0
VIEW_SIMULATION ?= 0

WINEPREFIX := ${HOME}/.wineprefix/wincupl

CUPL_OUT_JEDEC         := -j
CUPL_OUT_HEX           := -h
CUPL_OUT_HL            := -i
CUPL_CREATE_ABSOLUTE   := -a
CUPL_CREATE_LISTING    := -l
CUPL_CREATE_EXMACDEF   := -e
CUPL_CREATE_EXPRODEF   := -x
CUPL_CREATE_PDIF       := -p
CUPL_CREATE_BERK_PLA   := -b
CUPL_CREATE_PALASM     := -c
CUPL_DEACTIVATE_UNUSED := -d
CUPL_DISABLE_PROD_MERG := -r
CUPL_PROG_SEC_FUSE     := -g
CUPL_USE_SPEC_LIBRARY  := -u
CUPL_RUN_SIMULATION    := -s

CUPL_MIN_NONE          := -m0
CUPL_MIN_QUICK         := -m1
CUPL_MIN_QUINE_MCCLUS  := -m2
CUPL_MIN_PRESTO        := -m3
CUPL_MIN_EXPRESSO      := -m4

CUPL_MICROSOFT_ERRORS  := q

CUPL_MINIMIZATION ?= ${CUPL_MIN_QUICK}

DO_RUN_SIM = $(filter-out ${RUN_SIMULATION},0)
DO_VIEW_SIM = $(filter-out ${VIEW_SIMULATION},0)

CUPL_FLAGS = \
	$(if ${DO_RUN_SIM},${CUPL_RUN_SIMULATION}) \
	${CUPL_DEACTIVATE_UNUSED} ${CUPL_MINIMIZATION} \
	${CUPL_CREATE_BERK_PLA} ${CUPL_CREATE_EXPRODEF} ${CUPL_CREATE_ABSOLUTE}

CUPL_LIBRARY ?= 'C:\Wincupl\Shared\Atmel.dl'

FITTER_NAME ?= $(shell ../find_fitter.py ${PROJECT}.PLD)

.PHONY: all clean mrproper

all: ${PROJECT}.jed $(if ${DO_RUN_SIM},${PROJECT}.vcd)

ifeq (${FITTER_NAME},DIRECT)
%.tt2: %.PLD
	WINEPREFIX=${WINEPREFIX} wine 'C:\Wincupl\Shared\cupl.exe' \
	${CUPL_FLAGS} ${CUPL_OUT_JEDEC} ${CUPL_USE_SPEC_LIBRARY} ${CUPL_LIBRARY} $^

%.jed: %.tt2
	touch $@

else

FITTER ?= 'C:\Wincupl\WinCupl\Fitters\${FITTER_NAME}'

%.tt2: %.PLD
	WINEPREFIX=${WINEPREFIX} wine 'C:\Wincupl\Shared\cupl.exe' \
	${CUPL_FLAGS} ${CUPL_USE_SPEC_LIBRARY} ${CUPL_LIBRARY} $^

%.jed: %.tt2
	WINEPREFIX=${WINEPREFIX} wine ${FITTER} $^
endif

%.wo: %.tt2
	touch $@

%.vcd: %.wo
	../wo_to_vcd.py < $^ > $@
	$(if ${DO_VIEW_SIM},gtkwave $@)

mrproper: clean
	rm -f ${PROJECT}.jed

clean:
	rm -f ${PROJECT}.abs
	rm -f ${PROJECT}.doc
	rm -f ${PROJECT}.fit
	rm -f ${PROJECT}.io
	rm -f ${PROJECT}.pin
	rm -f ${PROJECT}.pla
	rm -f ${PROJECT}.tt2
	rm -f ${PROJECT}.tt3
	rm -f ${PROJECT}.sim
	rm -f ${PROJECT}.so
	rm -f ${PROJECT}.wo
	rm -f ${PROJECT}.vcd
