#!/usr/bin/python3

import sys
import re

FITTERS = [
        ("f1502", "fit1502.exe"),
        ("f1504", "fit1504.exe"),
        ("g16v8a", "DIRECT"),
]

if len(sys.argv) != 2:
    print(f"usage: {sys.argv[0]} file.pld")
    sys.exit(1)

DEV_RE = re.compile("^device\s+([^;]*)\s*;?$")

with open(sys.argv[1]) as f:
    for line in f:
        line = line.strip().lower()
        m = DEV_RE.match(line)
        if m is not None:
            dev = m.group(1)

            for devname, fitter in FITTERS:
                if dev.startswith(devname):
                    print(fitter)
                    sys.exit()
            print("error: can't find fitter for", dev)
            sys.exit(2)

    print("error: can't find device statement")
    sys.exit(3)
