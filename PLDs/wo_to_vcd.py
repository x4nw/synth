#!/usr/bin/python3

import sys

print("$timescale 1ns $end")

signals = []
first_dump = True

def emit_signals(line):
    global first_dump
    header, time, values = line.split()

    # Emit two lines for every 1 in the .wo, so we can make a clock

    for k in range(2):
        time_vcd = 2*int(time) - 2 + k

        print(f"#{time_vcd}")

        # Change values:
        #  P -> 0
        #  C -> 0, 1
        #  * -> U

        if first_dump:
            print("$dumpvars")
        for n, i in enumerate(values):
            if i == "P":
                i = "0"
            elif i == "C":
                i = "0" if k == 1 else "1"
            elif i == "*":
                i = "U"
            print(f"{i}{signals[n][1]}")
        if first_dump:
            print("$end")
        first_dump = False

order_entries = []
order_done = False
generated_vars = False

for line in sys.stdin:
    # ORDER can be split onto several lines. Concatenate them, then
    # dump this info before the first #V
    if line.startswith("#H ORDER:"):
        sigs = line.strip().rstrip(";")[10:].split(",")
        order_entries.extend(i.strip() for i in sigs)
        order_done = ";" in line
    elif line.startswith("#H ") and order_entries and not order_done:
        sigs = line.strip().rstrip(";")[3:].split(",")
        order_entries.extend(i.strip() for i in sigs)
        order_done = ";" in line

    elif line.startswith("#V"):
        if not generated_vars:
            for n, i in enumerate(order_entries):
                i = i.strip()
                if not i:
                    continue
                token = "i"+str(n)
                signals.append((i, token))
                print(f"$var wire 1 {token} {i} $end")
            print("$enddefinition $end")
            generated_vars = True
        emit_signals(line)
